# Global State Management

[Grabación Session 7](https://drive.google.com/file/d/1EJEgOeedEiy-St1Kw76ZPZhNzw_5SA6L/view?usp=sharing)
([Transcript](https://drive.google.com/file/d/16QfS7ivuh7Se2WSyOz9_7bnl1R-Tb9Ub/view?usp=sharing))

- [Manejo de estados globales con React Server Components)](https://stackoverflowteams.com/c/edifycr/articles/1732)
- [Redux Essentials](https://redux.js.org/tutorials/essentials/part-1-overview-concepts)
  - [Redux Toolkit](https://redux-toolkit.js.org/introduction/getting-started)
    - [Redux Toolkit setup with Next.js](https://redux-toolkit.js.org/usage/nextjs)
    - [RTK with Typescript](https://redux-toolkit.js.org/usage/usage-with-typescript)
    - [RTK Query](https://redux-toolkit.js.org/rtk-query/overview)
- [Zustand](https://github.com/pmndrs/zustand)
  - [Demo](https://zustand-demo.pmnd.rs/)
- [MobX](https://mobx.js.org/README.html)
- [Recoil](https://recoiljs.org/)
- [Tutorial](https://www.pronextjs.dev/tutorials/state-management/intro-to-state-management-with-next-js-app-router)

![alt text](global-state-management.jpg "Title")

## Ejercicios

Usando como base el ejemplo todo_testing_app, migre la implementación del global state management de React.Context a una biblioteca como Redux, Zustand, MobX, Recoil o Jotai

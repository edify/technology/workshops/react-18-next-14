import { TaskList } from "@/app/components/TaskList/TaskList";
import Typography from '@mui/material/Typography';

export default function Main() {
  return (
    <>
      <Typography sx={{ mt: 4, ml: 9 }} variant="h4" color="text.primary">
        Viewing all tasks:
      </Typography>
      <TaskList />
    </>
  );
}
import React from 'react';
import { AppProps } from 'next/app';
import { TodoAppProvider } from "@/app/store/AppProvider";

const MyApp: ({Component, pageProps}: { Component: any; pageProps: any }) => JSX.Element = ({ Component, pageProps }) => {
    return (
      <TodoAppProvider>
        <Component {...pageProps} />
      </TodoAppProvider>
    );
};

export default MyApp;

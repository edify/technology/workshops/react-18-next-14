import React, { FC, useContext } from "react";
import { ITask } from '../../interfaces/task.interface';
import { useRouter } from 'next/router';
import { TodoAppContext } from "@/app/store/AppProvider";
import Checkbox from '@mui/material/Checkbox';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export interface ITaskProps {
    task: ITask;
}
export const EditTask: FC<ITaskProps> = ({ task }) => {
    const router = useRouter();
    const { onCompleteTask, onDeleteTask, onEditTitle } = useContext(TodoAppContext);

    const handleOnComplete = () => {
        onCompleteTask(task.id);
    }

    const goBack = () => {
        router.replace(`/`)
    }

    const handleDelete = () => {
        onDeleteTask(task.id);
        goBack();
    };

    const handleInputChange = (event: any) => {
        onEditTitle(task.id, event.target.value);
    }

    return (
        <>
            <Card sx={{ width: 370, mb: 3, ml: 9, mt: 5, backgroundColor: "#eeeeee" }}>
                <CardContent>
                    <Typography data-testid={`task-id-${task.id}`} sx={{ fontSize: 14 }} color="text.secondary">
                        Id: {task.id}
                    </Typography>
                    <TextField
                        style={{ width: 330, marginTop: 15 }}
                        id="filled-multiline-static"
                        label="Title"
                        multiline
                        defaultValue={task.title}
                        variant="filled"
                        onChange={handleInputChange}
                        data-testid={`title-input`}
                    />
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={goBack}>BACK</Button>
                    <Button color="error" size="small" onClick={handleDelete}>DELETE</Button>
                    <Typography color="text.secondary">
                        Completed:
                    </Typography>
                    <Checkbox data-testid={'on-complete-checkbox'} checked={task.isCompleted}
                        onChange={handleOnComplete} />
                </CardActions>
            </Card>
        </>
    )
}
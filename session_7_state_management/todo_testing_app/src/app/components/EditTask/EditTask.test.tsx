import React from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from "@testing-library/react";
import { EditTask } from "./EditTask";
import { TodoAppContext } from '@/app/store/AppProvider';
import { createAppProviderMock } from '../../mocks/appProvider.mock'
import { createTaskMock } from '../../mocks/task.mock';

const replace = jest.fn();
jest.mock('next/router', () => ({
    useRouter() {
        return {
            replace,
            pathname: '',
        };
    },
}));

describe('EditTask Component', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    const taskMock = createTaskMock();
    const appProviderMock = createAppProviderMock({ tasks: [taskMock] });

    it('should render initial state', () => {
        render(
            <TodoAppContext.Provider value={appProviderMock}>
                <EditTask task={taskMock} />
            </TodoAppContext.Provider>
        );
        const id = screen.getByTestId(`task-id-${taskMock.id}`);
        const title = screen.getByText(`${taskMock.title}`);
        const backButton = screen.getByText('BACK');
        const deleteButton = screen.getByText('DELETE');
        const checkbox = screen.getByTestId('on-complete-checkbox');

        expect(id).toBeInTheDocument();
        expect(title).toBeInTheDocument();
        expect(backButton).toBeInTheDocument();
        expect(deleteButton).toBeInTheDocument();
        expect(checkbox).toBeInTheDocument();
    });

    it('should click the back button', () => {
        render(
            <TodoAppContext.Provider value={appProviderMock}>
                <EditTask task={taskMock} />
            </TodoAppContext.Provider>
        );
        const backButton = screen.getByText('BACK');
        expect(backButton).toBeInTheDocument();

        fireEvent.click(backButton);
        expect(replace).toHaveBeenCalledWith(`/`);
    });

    it('should click the delete button', () => {
        render(
            <TodoAppContext.Provider value={appProviderMock}>
                <EditTask task={taskMock} />
            </TodoAppContext.Provider>
        );
        const deleteButton = screen.getByText('DELETE');
        expect(deleteButton).toBeInTheDocument();

        fireEvent.click(deleteButton);
        
        expect(replace).toHaveBeenCalledWith(`/`);
        expect(appProviderMock.onDeleteTask).toHaveBeenCalled();
    });

    it('should click the checkbox', () => {
        render(
            <TodoAppContext.Provider value={appProviderMock}>
                <EditTask task={taskMock} />
            </TodoAppContext.Provider>
        );
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox).toBeInTheDocument();

        fireEvent.click(checkbox);
        expect(appProviderMock.onCompleteTask).toHaveBeenCalled();
    });

    it('should update the title', () => {
        render(
            <TodoAppContext.Provider value={appProviderMock}>
                <EditTask task={taskMock} />
            </TodoAppContext.Provider>
        );

        const titleInput = screen.getByTestId('title-input');
        expect(titleInput).toBeInTheDocument();

        const textarea = titleInput.querySelectorAll('textarea')[0];
        expect(textarea).toBeInTheDocument();

        fireEvent.change(textarea, { target: { value: 'New Task Title' } });
        expect(appProviderMock.onEditTitle).toHaveBeenCalled();
    });
});
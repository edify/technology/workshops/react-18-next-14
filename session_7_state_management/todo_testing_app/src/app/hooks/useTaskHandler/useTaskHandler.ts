import { useEffect, useState } from "react";
import { ITask } from '../../interfaces/task.interface';

export const useTaskHandler = () => {
    const [tasks, setTasks] = useState<ITask[]>([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                const parsedTasks = json.map(({ id, title }: { id: number, title: string }) => ({ id, title, isCompleted: false }));
                setTasks(parsedTasks);
            })
            .catch(error => console.error('Error fetching tasks:', error));
    }, []);


    const onDeleteTask = (taskId: number) => {
        setTasks(tasks.filter(task => task.id !== taskId));
    };

    const onCompleteTask = (taskId: number) => {
        setTasks(tasks.map(task =>
            task.id === taskId ? { ...task, isCompleted: !task.isCompleted } : task
        ));
    };

    const onFindTask = (taskId: number) => {
        return tasks.find((task) => task.id === taskId);
    };

    const onEditTitle = (taskId: number, newTitle: string) => {
        setTasks((prevTasks) =>
            prevTasks.map((task) => task.id === taskId ? { ...task, title: newTitle } : task));
    };

    return {
        tasks,
        onDeleteTask,
        onCompleteTask,
        onFindTask,
        onEditTitle
    }
}

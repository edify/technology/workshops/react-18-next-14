module.exports = {
    rootDir: './',
    coverageReporters: ['lcov', 'html'],
    testEnvironment: 'jest-environment-jsdom',
    testMatch: ['**/*.(test).(ts|tsx)'],
    transform: {
        '^.+\\.(ts|tsx)$': ['@swc/jest'],
    },
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/src/$1"
    }
}
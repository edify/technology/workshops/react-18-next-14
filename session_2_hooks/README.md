## Session 2: Building a To-Do App

[Session 2 Recording](https://drive.google.com/file/d/1MHwuj_2TQSapkioG5g7DuZcuijiZI4Uv/view)

[Session 2 Transcript](https://drive.google.com/file/d/1xWg8wRoHDnh8VPX7yMkDEhY7fuYrVvpi/view)

This workshop will guide you through building a simple To-Do application using various React concepts. We'll cover:

* **Rendering Elements:**  Creating reusable components for tasks and the main application.
* **Conditional Rendering:**  Displaying or hiding tasks based on completion status.
* **Lists:**  Iterating through tasks and rendering them individually.
* **Event Handling:**  Adding, editing, and deleting tasks using user interactions.
* **useState:** Managing the list of tasks and their completion status as state.
* **useRef:** (Optional) Keeping track of the input field for adding new tasks.
* **useMemo/useCallback:** Optimizing performance for frequently used functions or components.
* **useEffect:**  Fetching data from https://jsonplaceholder.typicode.com/ and performing side effects after state changes.
* **useContext:** (Optional)  (Consider including this if your application requires complex state management across components)
* **Custom Hooks:**  Building a reusable hook for adding new tasks.
* **Introduction to Next.js App Router:**  basic routing between the To-Do list and a completed tasks view.

**What you'll need:**

* Basic understanding of React concepts (components, props, state)
* Code editor (e.g., Visual Studio Code)
* A React development environment set up (Consider using Create React App for a quick start)

**Session Duration:** 1 hour

**Workshop Outline:**

**Part 1: Setting Up the Project**

1. Create a new React project using Next.js
   1. Select Typescript
   2. Select App Router
   3. Select `/src` directory
```
npx create-next-app@latest
```
2. Install any additional libraries needed
   1. Design system
   2. Axios

**Part 2: Building Components (30 minutes)**

1. **Task Component:** Create a component `Task.js` to display an individual task with its title, completion checkbox, and edit/delete buttons. Use props to receive the task data and handle events.
2. **To-Do List Component:** Create a component `TodoList.js` to manage the list of tasks. Use `useState` to store the task data.
   1. Retrieve `/todos` from [JSONPlaceholder](https://jsonplaceholder.typicode.com/) using an `useEffect`hook. Remember to set the dependencies array. DO NOT call the async function from within the `useEffect`hook.
      1. Use `https://jsonplaceholder.typicode.com/todos` endpoint. You can base your code on this [guide](https://jsonplaceholder.typicode.com/guide/)
      2. If your application falls into an infinite loop, maybe you need `useCallback` hooks for your event functions.
   2. Render the `Task` component for each item in the list.
   3. Implement functionality using event handlers to mark tasks as complete/incomplete.
      1. I don't think *JSON Placeholder* provides that endpoint, if not, do the changes in your state.
      2. **Remember**: states in React are immutable, always take this into account. You might need to use an `useReducer` hook to ease your work, but you may successfully complete this step with `useState`.
   4. Use conditional rendering to display only active or completed tasks (you can use a button, toggle button, etc.).
3. 3. **useMemo/useCallback:** Explore using `useMemo` or `useCallback` to optimize performance for frequently used functions or components within the To-Do list. The idea is to learn how to use them, don't worry if you think using them is an overkill.
4. **useContext:** (Optional)  If your application requires complex state management across components, consider implementing a simple state management solution using `useContext` to share task data across the To-Do list and completed tasks view (if using routing).
5. **Custom Hooks:** (Optional)  Create a custom hook for adding new tasks, encapsulating the logic and improving code reusability. Extract some of your work into a custom hook if it's possible.
6. Make a separated page for a single task view using [Next.js App Router](https://nextjs.org/docs/app/building-your-application/routing)
   1. Add a button for each element in the list to be able to navigate to each single task view
   2. Add a button in the single task view to be able to go back to the TodoList page.
   3. Add the possibility to *complete* or *un-complete* the current task
      1. Maybe this functionality could be your custom hook and maybe you'll need `useContext` here 😉
   4. (Optional) Add a navigation bar to be able to navigate to any other task from the single task view.

**Part 3: Wrap-up and Discussion**

* Discuss challenges faced during the workshop.
* Explore additional features and functionalities you can add to the To-Do application.

**Remember:**

* This is a practice session. Focus on understanding the concepts and implementing functionality in a basic way.
* The provided outline is a guide. Adjust the pace and depth based on your group's experience.
* Encourage exploration and experimentation with the code.
* Refer to the official React documentation and online resources for further details on each concept.

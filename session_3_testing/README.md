# Session 3: Testing To-Do App

[Session 3 Recording](https://drive.google.com/file/d/19hrBLQJ0-KJIn5Vl7R0BjdGoS5Uo63DJ/view?usp=sharing)

[Session 3 Transcript](https://drive.google.com/file/d/1nVkBIjkzEbQnuCT0gkExojZ2QQXX2ZRV/view?usp=sharing)

This workshop will guide you through testing a simple To-Do application. We'll cover:

* **Testing using JEST and testing library:**  Creating unit tests using different functionalities from JEST and testing library.
* **Test react components:**  Testing react components (render).
* **Test react hooks:**  Testing logic and functionalities.
* **Mocks:**  Creating mocks to test components.


**What you'll need:**

* Code editor (e.g., Visual Studio Code).
* A React development environment set up (Consider using Create React App for a quick start).
* Basic application previously created.

**Session Duration:** 1 hour

**Workshop Outline:**

**Part 1: Setting Up the Project**

1. Install JEST and testing library dependencies.
```
npm install --save-dev jest
```
```
npm install --save-dev @testing-library/react @testing-library/jest-dom
```

2. Configure Jest using SWC (Speedy Web Compiler)
```
npm install --save-dev jest @swc/core @swc/jest
```
3.  Jest Configuration:
Create **jest.config.js** In your project's root directory,
create a file named jest.config.js (or jest.config.mjs for modern JavaScript).
Add the following configuration:
```
module.exports = {
    rootDir: './',
    coverageReporters: ['lcov', 'html'],
    testEnvironment: 'jest-environment-jsdom',
    testMatch: ['**/*.(test).(ts|tsx)'],
    transform: {
        '^.+\\.(ts|tsx)$': ['@swc/jest'],
    },
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/src/$1"
    }
}
```


**Part 2: Testing Components**

1. **Task Component:** Create a file `Task.test.tsx` to test the component.
2. **To-Do List Component:** Create a file `TodoList.test.tsx` to test the component.
3. **Hooks:** Create files to test hooks.
4. **Context** Create files to test contexts.
5. **Mocks** Create mocks to simplify unit testing.
   1. TaskMock.
   2. ContextMock.

**Part 3: Wrap-up and Discussion**

* Discuss challenges faced during the workshop.
* Explore additional concepts and tests you can add to the To-Do application.

**Remember:**

* This is a practice session. Focus on understanding the concepts and implementing testing in a basic way.
* The provided outline is a guide. Adjust the pace and depth based on your group's experience.
* Encourage exploration and experimentation with the code.
* Refer to the official JEST and testing-library documentation and online resources for further details on each concept.

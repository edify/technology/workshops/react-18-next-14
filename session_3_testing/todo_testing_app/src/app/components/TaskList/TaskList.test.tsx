import React from 'react';
import '@testing-library/jest-dom';
import { render, screen } from "@testing-library/react";
import { TaskList } from "./TaskList";
import { TodoAppContext } from '@/app/store/AppProvider';
import { createAppProviderMock } from '../../mocks/appProvider.mock'

const replace = jest.fn();
jest.mock('next/router', () => ({
    useRouter() {
        return {
            replace,
            pathname: '',
        };
    },
}));

describe('TaskList Component', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render initial state', () => {
        render(
            <TodoAppContext.Provider value={createAppProviderMock()}>
                <TaskList />
            </TodoAppContext.Provider>
        );
        const task1 = screen.getByTestId(`task-id-1`);
        const task2 = screen.getByTestId(`task-id-2`);
        const task3 = screen.getByTestId(`task-id-3`);

        expect(task1).toBeInTheDocument();
        expect(task2).toBeInTheDocument();
        expect(task3).toBeInTheDocument();
    });

    it('should render initial state without tasks', () => {
        render(
            <TodoAppContext.Provider value={createAppProviderMock({tasks: []})}>
                <TaskList />
            </TodoAppContext.Provider>
        );
        expect(screen.queryByTestId('task-id-1')).not.toBeInTheDocument();
        expect(screen.queryByTestId('task-id-2')).not.toBeInTheDocument();
        expect(screen.queryByTestId('task-id-3')).not.toBeInTheDocument();

        const errorText = screen.getByText('Nothing to show...');
        expect(errorText).toBeInTheDocument();
    });
});
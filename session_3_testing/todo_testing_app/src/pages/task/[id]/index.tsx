'use client';
import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router';
import { TodoAppContext } from "@/app/store/AppProvider";
import { ITask } from '@/app/interfaces/task.interface';
import { EditTask } from "@/app/components/EditTask/EditTask";
import Typography from '@mui/material/Typography';

export default function ViewTask() {
  const router = useRouter();
  const { tasks, onFindTask } = useContext(TodoAppContext);
  const [editingTask, setEditingTask] = useState<ITask | undefined>(undefined);

  useEffect(() => {
    const id = router.query?.id?.toString() ?? "";
    if (id) {
      const task = onFindTask(Number(id));
      if (task) {
        setEditingTask(task);
      }
    }
  }, [tasks, onFindTask, router])

  return (
    <>
      {editingTask ? (
        <>
          <Typography sx={{ mt: 4, ml: 9 }} variant="h4" color="text.primary">
            Editing task:
          </Typography>
          <EditTask task={editingTask}/>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </>
  );
}

# Session 6: Asynchronous Calls

This module will explain:
- react-query (now it is called **tanstack query**)
- Fetch Data
- Call endpoints (GET, POST, PUT, etc)

Record of [Session 5, Asynchronous Calls](https://drive.google.com/file/d/1z4aR-C-qlWPDlI0V7l6Hwnu77REKsflb/view?pli=1)

## Main concepts

- **React-query:** It's a library for managing server-state in React applications. It simplifies data fetching, caching, synchronization, and updates, reducing the need for complex state management solutions and manual handling of asynchronous operations.

This is the [official documentation](https://tanstack.com/query/latest/docs/framework/react/overview)

## What you will need:

**API:**
```
https://jsonplaceholder.typicode.com/users
```

**Responses of useQuery**


These are the [responses of useQuery](https://tanstack.com/query/latest/docs/framework/react/reference/useQuery) that you will recive when use `useQuery`, this is the structure:
```
const {responses} = useQuery([firstArg], asyncFunctionQuery);
```
The `firstArg`, it is an array to manage the cache

**Intallations**

*React-query*
```
npm install react-query
```

"use client";
import React, { useState } from "react";
import './fetchData.styles.css';

const FetchData = () => {
    const [userNames, setUserNames] = useState<string[]>([]);
    const [error, setError] = useState<string>();

    const getUsernames = async () => {
        try {
            const res = await fetch('https://jsonplaceholder.typicode.com/users');
            const userData = await res.json();
            const arrayUserNames = userData?.map((currentUser: { username: any; }) => currentUser.username);
            setUserNames(arrayUserNames);
        } catch (error) {
            setError(`${error}`);
        }
    };

    const clearUserNames = () => {
        setUserNames([]);
    }

    return (
        <div className="container">
            <h1>With Fetch</h1>
            <button onClick={getUsernames} className="btn-get-usernames">Using Feth Data</button>
            <br />
            {error && <div>Hay un error: <strong>{error}</strong></div>}
            {userNames.length > 0 && !error && (
                <div >
                    <button onClick={clearUserNames}>Clear usernames</button>
                    <ul>
                        {userNames.map((userName) => <li>{userName}</li>)}
                    </ul>
                </div>
            )}
        </div>
    );
};

export default FetchData;
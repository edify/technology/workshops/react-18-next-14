"use client";
import React from "react";
import { useQuery } from 'react-query';
import './withReactQuery.styles.css';

const getNames = async () => {
    try {
        const res = await fetch('https://jsonplaceholder.typicode.com/users');
        const userData = await res.json();
        return userData?.map((currentUser: { username: any; }) => currentUser.username);
    } catch (error) {
        throw new Error('Hubo un error');
    }
}

const WithReactQuery   : React.FC = () => {
    const { data, isLoading, isError, error, refetch } = useQuery(['names'], getNames);

    const refresData = () => {
        refetch();
    };

    return (
        <div className="container">
            <h1>With React Query</h1>
            <br />
            {!isLoading && isError && (<p><strong>{`${error}`}</strong></p>)}
            {isLoading && data ?
                (<span>Cargando</span>) :
                (<ul>
                    {data?.map((currentData: string) => <li>{currentData}</li>)}
                </ul>)
            }
            <br />
            <button onClick={refresData} className="btn-get-names">Refresh Names</button>
        </div>);
}

export default WithReactQuery;
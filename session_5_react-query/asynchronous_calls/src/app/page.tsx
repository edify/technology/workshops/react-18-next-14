"use client";
import styles from "./page.module.css";
import FetchData from "@/components/fetchData";
import WithReactQuery from "@/components/withReactQuery";
import { QueryClient, QueryClientProvider } from "react-query";

// Query client, here we indicate how this will work, the default values it will have
const queryClient = new QueryClient();

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.left}>
        <FetchData />
      </div>
      <div className={styles.right}>
        {/* Provider */}
        <QueryClientProvider client={queryClient}>
          <WithReactQuery />
        </QueryClientProvider>
      </div>
    </main>
  );
}

# Session 1: Introduction to React.js

[Session 1 Recordings](https://drive.google.com/file/d/1ptS1U3clgwzmhTa2S56D4ejlM1dk-o6v/view?usp=sharing)

[Session 1 Transcripts](https://drive.google.com/file/d/1OvCfg78bIrgOfDVzWyOuV6jdREy_sJvE/view?usp=sharing)

* What is React and JSX?
* Components: Building Blocks of React Applications
* Setup React with Vite
* Setup IDE
* Environment (nvm)
* State Management in React (useState hook)
* Props: Passing Data Between Components

## Hands-on Lab

1. Setup Environment
```
node -v
```
[Install nvm](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating)

2. Setup IDE

* WebStorm
* VSCode

3. Create a React.js application
```
npm create vite@latest
```

```
npx create-next-app@latest
```

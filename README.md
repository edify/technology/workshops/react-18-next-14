# React.js Workshops

## Structure

* **Session Length:** 1 hour
* **Format:** Each session will have two parts:
  * **Theory (30 minutes):** Introduce the concepts through presentations, code examples, and discussions.
  * **Hands-on Lab (30 minutes):** Apply the learned concepts in practical coding exercises.

## Part 1: React Fundamentals


### Session 1: Introduction to React.js

* What is React and JSX?
* Components: Building Blocks of React Applications
* Setup React with Vite
* Setup IDE
* Environment (nvm)
* State Management in React (useState hook)
* Props: Passing Data Between Components


### Session 2: Building User Interfaces (Routing)

* Rendering Elements
* Conditional Rendering and Lists
* Event Handling in React
* Introduction to React Router and Application Routing (Next.js)


### Session 3: Hooks Deep Dive (React 16+)

* useState, useRef, useMemo, useCallback, useEffect, useContext
* Custom Hooks for Reusable Logic
* Managing Side Effects with Hooks


### **Session 4: Introduction to Testing in React (Vitest & Testing Library)**

* Importance of Testing in React Development
* Setting Up a Testing Environment (Vitest)
* Writing Unit Tests with React Testing Library


### Session 5:  Asynchronous Calls

* Fetch Data
* Call endpoints (GET, POST, PUT, etc)
* react-query

### Session 6: Forms and Data Handling

* Controlled Components and Uncontrolled Components
* Handling Form Submissions
* Validating form input (zod, joi, yup)
* Error handling

## Part 2: Intermediate React

### Session 7: Introduction to Global State Management

* Global store or not?
  * The three rules for global store management in Next.js 13+
* Introduction to Redux Toolkit Concepts (Store, Actions, Reducers)
* Integrating Redux with React Applications
* Integrating with Zuztand

### Session 8: React Optimization Techniques
- Performance improvements
  - Code Splitting
  - Lazy Loading
  - Dynamic imports
- Introduction to Concurrent Mode and Suspense
  - Suspense
  - Performance Optimization Strategies
    - Memoization
    - React Compiler
- Automatic Batching and New Rendering Behavior


### Part 3: Advanced React and New Features

### Session 9: React 18 New Features
* Automatic Batching and New Rendering Behavior

### Session 10: Advanced Topics & Deep Dives
* Error Handling and Boundary Components
* React Server Components
* Server-Side Rendering (SSR) with Next.js (or similar framework)
* Using the useLayoutEffect Hook


## Resources

* **Official React Documentation:**[ https://legacy.reactjs.org/docs/getting-started.html](https://legacy.reactjs.org/docs/getting-started.html)
* **React Tutorial:**[https://app.pluralsight.com/paths/skill/react-18](https://react.dev/learn/tutorial-tic-tac-toe)
* **Pluralsight React 18 Path:**[https://app.pluralsight.com/paths/skill/react-18](https://app.pluralsight.com/paths/skill/react-18)

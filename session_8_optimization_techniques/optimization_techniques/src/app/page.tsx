import {ClientComponent} from "@/components/ClientComponent";
import {ServerComponent} from "@/components/ServerComponent";

import React, {Suspense} from "react";



export default function Home() {
  return (
    <main className="m-4 flex justify-center items-center w-full">
      <Suspense fallback={<h1>Loading...</h1>}>
        <ServerComponent />
      </Suspense>
    </main>
  );
}

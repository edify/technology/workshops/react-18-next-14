'use client';

import React, {useState, useEffect, useMemo} from 'react';
import {fetchAllTasks, Task} from "@/actions/tasks.actions";

type ClientComponentProps = {
  tasks: Task[];
}

export function ClientComponent(props: ClientComponentProps) {
  const [counter, setCounter] = React.useState(1);
  const [reload, setReload] = React.useState(1);

  // This line should or shouln't be memoized?
  const memoValue = (new Date()).getTime();
  const [tasks, setTasks] = useState<Task[]>(props.tasks);

  const loadTasks = async () => {
    const tasks = await fetchAllTasks();
    setTasks(tasks);
  };

  // useEffect(() => {
  //   loadTasks();
  //   // fetch('https://jsonplaceholder.typicode.com/posts')
  //   //   .then(response => response.json())
  //   //   .then(json => {
  //   //     const parsedTasks = json.map(({ id, title }: { id: number, title: string }) => ({ id, title, isCompleted: false }));
  //   //       setTasks(parsedTasks);
  //   //     })
  //   //   .catch(error => console.error('Error fetching tasks:', error));
  // }, [reload]);

  console.log('render');
  return (
    <div className="m-2 w-full">
      <p>{counter} - {memoValue}</p>
      <button type="button" onClick={() => loadTasks()}>Reload Tasks</button>
      <button
        onClick={() => {
          setCounter(counter + 1)
        }}
        className="middle none center rounded-lg bg-pink-500 py-3 px-6 font-sans text-xs font-bold uppercase text-white shadow-md shadow-pink-500/20 transition-all hover:shadow-lg hover:shadow-pink-500/40 focus:opacity-[0.85] focus:shadow-none active:opacity-[0.85] active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
        data-ripple-light="true"
      >Aumentar contador
      </button>
      <div className="m-6">

        <ul className="list-disc list-inside">
          {tasks.map(({id, title}: { id: number, title: string }) => (
            <li key={id}>{title}</li>
          ))}
        </ul>
      </div>

    </div>
  )
}

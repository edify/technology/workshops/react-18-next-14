import React from 'react';
import {ClientComponent} from "@/components/ClientComponent";
import {fetchAllTasks} from "@/actions/tasks.actions";


export async function ServerComponent() {
  const tasks = await fetchAllTasks();
  return (
    <div className="server-component">
      <ClientComponent tasks={tasks} />
    </div>
  )
}

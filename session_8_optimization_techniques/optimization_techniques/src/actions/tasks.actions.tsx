'use server';
function timeout(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


export type Task = {
  id: number,
  title: string,
  isCompleted: boolean,
}

export async function fetchAllTasks(): Promise<Task[]> {
  await timeout(3000);
  const response = await fetch('https://jsonplaceholder.typicode.com/posts')
  return await response.json();
}

# Session 8: React Optimization Techniques

[Recording](https://drive.google.com/file/d/1uDd9wnsOvb1fBJutSEwoflS-EEBM77XZ/view?usp=sharing)

- Dynamic loading
  - Code Splitting
  - Lazy Loading
  - Dynamic imports
  - Suspense
- Performance Optimization Strategies
  - Memoization
  - React Compiler
- Automatic Batching and New Rendering Behavior



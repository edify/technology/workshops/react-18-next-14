'use client'

import {useState, useActionState} from "react";
import {createPost, Post} from "@/postService";
import { useFormStatus } from "react-dom";
import {object, string} from "yup";

function timeout(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function Buttons () {
  const {pending, ...status} = useFormStatus();
  return (
    <fieldset>
      {pending && (<em>Submitting...</em>)}
      <button
        type="button"
        disabled={pending}
        className="mr-3 inline-block px-6 py-2 text-xs font-medium leading-6 text-center text-black uppercase transition bg-gray-100 rounded shadow ripple hover:shadow-lg hover:bg-gray-200 focus:outline-none"
      >
        Cancel
      </button>
      <button
        type="submit"
        disabled={pending}
        className="inline-block px-6 py-2 text-xs font-medium leading-6 text-center text-white uppercase transition bg-blue-700 rounded shadow ripple hover:shadow-lg hover:bg-blue-800 focus:outline-none"
      >
        Submit
      </button>
      <em>Form Status: {JSON.stringify(status)}</em>
    </fieldset>
  );
}

export default function Home() {

  const [posts, setPosts] = useState<Post[]>([]);

  async function createPostAction (previousState: Post, formData: any) {
    console.log('previousState', previousState);

    let postSchema = object({
      title: string().required(),
      body: string().required().max(20),
    });

    let validatedFormData;

    try {
      validatedFormData = await postSchema.validate({
        title: formData.get('title'),
        body: formData.get('body'),
      });
    }catch(e) {
      debugger;
      console.error(e);
    }
    console.log('validatedFormData', validatedFormData);
    const newPost = await createPost(validatedFormData as Post);
    setPosts([...posts, newPost]); // fuente inagotable de errores a futuro
    return newPost;
  }

  const [state, formAction] = useActionState(createPostAction, {title: 'a', body: 'b'});

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1>Posts</h1>
      {state.error && <em>Something went wrong</em>}
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono lg:flex">
        <em>{JSON.stringify(state)}</em>
        <form action={formAction}>
          <fieldset className="mb-3">
            <label htmlFor="title" className="mb-3 block text-base font-medium text-[#07074D]">Title</label>
            <input id="title" name="title" type="text"
                   className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"/>
          </fieldset>

          <fieldset className="mb-3">
            <label htmlFor="body" className="block text-base font-medium text-[#07074D]">Body</label>
            <input id="body" name="body" type="text"
                   className="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"/>
          </fieldset>

          <Buttons />

        </form>
      </div>
      <ul>
        {posts.map((post) => (
          <li key={post.id}>
            <h2>{post.title}: </h2>
            <p>{post.body}</p>
          </li>
        ))}
      </ul>
    </main>
  );
}

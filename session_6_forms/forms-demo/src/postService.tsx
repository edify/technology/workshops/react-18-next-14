'use server'

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export async function createPost(newPost: Post) {
  // console.log("window", window);
  const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify(newPost),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
  })
  return await response.json();
}

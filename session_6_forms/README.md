# Session 3: Testing To-Do App

[Session 6 Recording](#)

[Session 6 Transcript](#)

This workshop will guide you through testing a simple To-Do application. We'll cover:

* [Controlled and uncontrolled fields](https://react.dev/reference/react-dom/components/input#controlling-an-input-with-a-state-variable)
* Handling Form Submissions
  * [Form submissions on the client](https://react.dev/reference/react-dom/components/form#handle-form-submission-on-the-client)
  * [Form submissions with server actions](https://react.dev/reference/react-dom/components/form#handle-form-submission-with-a-server-action)
  * [Multiple submission types](https://react.dev/reference/react-dom/components/form#handling-multiple-submission-types)
* [useActionState](https://react.dev/reference/react/useActionState)
* [Optimistically updating form data](https://react.dev/reference/react-dom/components/form#optimistically-updating-form-data)
* Validating form input (zod, joi, yup)
* [Error handling](https://react.dev/reference/react-dom/components/form#display-a-form-submission-error-without-javascript)


**What you'll need:**

* Code editor (e.g., Visual Studio Code).
* A React development environment set up
* Basic application previously created.

